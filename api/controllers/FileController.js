/**
 * FileController
 *
 * @description :: Server-side logic for managing files
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  upload: function  (req, res) {

    let thedate = sails.moment().format("MMM Do YYYY h:mm:ss a");
    // sails.log( req.file('avatar') );
    req.file('file').upload({
        dirname: '../../assets/uploads',
        saveAs: req.file('file')._files[0].stream.filename // hakuna code yakadai \o/, nodejs should do better
    }, function (err, files) {
      if (err)
        return res.serverError(err);
      
      let thelink = '/uploads/' + files[0].filename;
      let thedescription = (req.param("description") == null) ? files[0].filename : req.param("description");

      File.create({
        name: files[0].filename,
        description: thedescription,
        date: thedate,
        link: thelink,
      }).exec(function (err, file){
        if (err) { return res.serverError(err); }

        sails.log('File created with id:' + file.id + ', name: ' + files[0].filename);
        return res.ok();
      });

      return res.json({
        message: files.length + ' file(s) uploaded successfully!',
        files: files,
        link: thelink,
        description: thedescription
      });
    });
    
  },

  results: function(req, res) {
    File.find()
    .sort('date DESC')
    .exec(function (err, allResults){
      if (err)
        return res.serverError(err);

      return res.json(allResults);
    });
  }

};

