/**
 * File.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    // Primitive attributes
    name: { type: 'string', defaultsTo: '' },
    description: { type: 'string', defaultsTo: '' },
    date: { type: 'string', defaultsTo: '' },
    link: { type: 'string' },
  }

};

